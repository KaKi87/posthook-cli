#!/usr/bin/env node

import vorpal from 'vorpal';
import axios from 'axios';
import AsciiTable from 'ascii-table';
import dayjs from 'dayjs';
import dayjsLocalizedFormat from 'dayjs/plugin/localizedFormat.js';

dayjs.extend(dayjsLocalizedFormat);

const
    cli = vorpal(),
    client = axios.create({
        baseURL: 'https://api.posthook.io/v1'
    }),
    refreshDelimiter = () => {
        const apiKey = cli.localStorage.getItem('apiKey');
        cli.delimiter(`${apiKey ? `${apiKey}@` : ''}posthook-cli$`);
    };

cli.localStorage('posthook-cli');

refreshDelimiter();

client.interceptors.request.use(config => ({
    ...config,
    headers: {
        ...config.headers,
        'X-API-Key': cli.localStorage.getItem('apiKey')
    }
}));

cli
    .command(
        'login <apiKey>',
        'Log client in'
    )
    .action(({ apiKey }, callback) => {
        cli.localStorage.setItem('apiKey', apiKey);
        refreshDelimiter();
        callback();
    });

cli
    .command(
        'logout',
        'Log client out'
    )
    .action((_, callback) => {
        cli.localStorage.removeItem('apiKey');
        refreshDelimiter();
        callback();
    });

cli
    .command(
        'list',
        'List hooks'
    )
    .option('--limit <limit>', 'Limit')
    .option('--status <status>', 'Filter by pending/completed/failed status')
    .option('--sortBy <sortBy>', 'Sort by creation/post date')
    .option('--sortOrder <sortOrder>', 'Sort asc/desc')
    .option('--minPostDate <minPostDate>', 'Filter by min post date')
    .option('--maxPostDate <maxPostDate>', 'Filter by max post date')
    .option('--minCreationDate <minCreationDate>', 'Filter by min creation date')
    .option('--maxCreationDate <maxCreationDate>', 'Filter by max creation date')
    .action(async ({
        options: {
            limit,
            status,
            sortBy,
            sortOrder,
            minPostDate,
            maxPostDate,
            minCreationDate,
            maxCreationDate
        }
    }, callback) => {
        try {
            const
                { data: { data } } = await client.get(
                    'hooks',
                    {
                        params: {
                            limit,
                            status,
                            sortBy: {
                                'creation': 'createdAt',
                                'post': 'postAt'
                            }[sortBy],
                            sortOrder: sortOrder ? sortOrder.toUpperCase() : undefined,
                            'postAtBefore': maxPostDate ? dayjs(maxPostDate).toISOString() : undefined,
                            'postAtAfter': minPostDate ? dayjs(minPostDate).toISOString() : undefined,
                            'createdAtBefore': maxCreationDate ? dayjs(maxCreationDate).toISOString() : undefined,
                            'createdAtAfter': minCreationDate ? dayjs(minCreationDate).toISOString() : undefined
                        }
                    }
                ),
                table = new AsciiTable();
            table.setHeading('ID', 'Creation date', 'POST date', 'Status');
            for(const {
                id,
                createdAt,
                postAt,
                tryAt,
                status
            } of data) table.addRow(
                id,
                dayjs(createdAt).format('L LT'),
                dayjs(postAt || tryAt).format('L LT'),
                status
            );
            console.log(table.toString());
        }
        catch(error){
            console.error(error);
        }
        callback();
    });

cli
    .command(
        'get <id>',
        'Get hook'
    )
    .action(async ({ id }, callback) => {
        try {
            const
                {
                    data: {
                        data: {
                            createdAt,
                            postAt,
                            tryAt,
                            status,
                            postDurationSeconds,
                            attempts,
                            data
                        }
                    }
                } = await client.get(`hooks/${id}`),
                table = new AsciiTable();
            table.addRow('Creation date', dayjs(createdAt).format('L LT'));
            table.addRow('POST date', `${dayjs(postAt || tryAt).format('L LT')}`);
            table.addRow('Status', `${status}${postDurationSeconds ? ` in ${postDurationSeconds.toFixed(2)}s` : ''}${attempts > 1 ? ` after ${attempts} attempts` : ''}`);
            console.log(table.toString());
            console.log(JSON.stringify(data, null, 4));
        }
        catch(error){
            console.error(error);
        }
        callback();
    });

cli
    .command(
        'add <path> <date>',
        'Add hook'
    )
    .option('--data <data>', 'POST data')
    .action(async ({
        path,
        date,
        options: {
            data
        }
    }, callback) => {
        try {
            const
                {
                    data: {
                        data: {
                            id,
                            createdAt,
                            postAt
                        }
                    }
                } = await client.post(
                    'hooks',
                    {
                        path,
                        'postAt': dayjs(date).toISOString(),
                        data
                    }
                ),
                table = new AsciiTable();
            table.addRow('ID', id);
            table.addRow('Creation date', dayjs(createdAt).format('L LT'));
            table.addRow('POST date', `${dayjs(postAt).format('L LT')}`);
            console.log(table.toString());
        }
        catch(error){
            console.error(error);
        }
        callback();
    });

cli
    .command(
        'delete <id>',
        'Delete hook'
    )
    .action(async ({ id }, callback) => {
        try {
            await client.delete(`hooks/${id}`);
        }
        catch(error){
            console.error(error);
        }
        callback();
    });

cli.show();