# `posthook-cli`

REPL CLI client for the [Posthook](https://posthook.io/) API.

```
  Commands:

    help [command...]            Provides help for a given command.
    exit                         Exits application.
    login <apiKey>               Log client in
    logout                       Log client out
    list [options]               List hooks
    get <id>                     Get hook
    add [options] <path> <date>  Add hook
    delete <id>                  Delete hook
```

Installation : `yarn global add git+https://git.kaki87.net/KaKi87/posthook-cli.git`